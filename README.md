# TimmRun

You can load timm's models in the pytorch environment for training and performance testing.

The program provides training, testing and classification of shuffled data.

By changing the values defined in the yaml file, you can use the function the way you want.

---
## How to use

1. Install pytorch and the timm library.

2. Change the variables.yml file to match your data.

3. Run the main.py file.