import os
import yaml
from tqdm.auto import tqdm
import PIL
import shutil

import torch
from torch.utils.data import DataLoader
from torch.optim import AdamW
from torch.optim.lr_scheduler import CosineAnnealingLR
from torchvision import datasets, transforms
import timm


def get_values(variables_file: str) -> dict:
    values = None
    if os.path.isfile(variables_file):
        with open(variables_file, 'r', encoding = 'utf-8') as file_descriptor:
            values = yaml.load(file_descriptor, Loader = yaml.FullLoader)
    return values
    

def get_device(gpu_index: int) -> str:
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    return device + ':' + str(gpu_index) if device == 'cuda' else device


def get_loader(data_path: str, transform, batch_size: int = 32, shuffle: bool = False) -> DataLoader:
    if transform == None:
        transform = transforms.Compose([transforms.Resize((224, 224)), transforms.ToTensor()])
    return DataLoader(dataset = datasets.ImageFolder(root = data_path, transform = transform),
        batch_size = batch_size, shuffle = shuffle)


def get_mean_std(data_loader: DataLoader) -> list:
    total_sum, total_square_root_sum, batch_count = 0.0, 0.0, len(data_loader)
    for data, _ in tqdm(data_loader, position = 0):
        total_sum += torch.mean(data, dim = [0, 2, 3])
        total_square_root_sum += torch.mean(data ** 2, dim = [0, 2, 3])
    total_mean = total_sum / batch_count
    total_std = (total_square_root_sum / batch_count - total_mean ** 2) ** 0.5
    return total_mean, total_std


def train(train_loader: DataLoader, device: str, epochs: int, name: str,
        model, criterion, optimizer, scheduler, printable: bool) -> float:
    model = model.to(device)
    criterion = criterion.to(device)
    model.train()
    last_accuracy = 0
    for epoch in range(1, epochs + 1):
        train_loss = 0
        train_accuracy = 0
        for data, targets in tqdm(train_loader, position = 0):
            data = data.to(device)
            targets = targets.to(device)

            output = model(data)
            loss = criterion(output, targets)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            train_loss += loss.item()
            predict = output.max(1)[1]
            train_accuracy += predict.eq(targets).sum().item()
        else:
            scheduler.step()
            train_loss /= len(train_loader)
            train_accuracy *= (100 / len(train_loader.dataset))
            last_accuracy = train_accuracy
            if printable:
                print(f'{name}: Epoch = {epoch} -> Loss = {train_loss:.4f}, Accuracy = {train_accuracy:.4f}%')
    return last_accuracy


def test(test_loader, device: str, name: str, model, criterion) -> float:
    model = model.to(device)
    criterion = criterion.to(device)
    model.eval()
    test_loss = 0
    test_accuracy = 0
    with torch.no_grad():
        for data, targets in tqdm(test_loader, position = 0):
            data = data.to(device)
            targets = targets.to(device)

            output = model(data)

            test_loss += criterion(output, targets).item()
            predict = output.max(1)[1]
            test_accuracy += predict.eq(targets).sum().item()
        else:
            test_loss /= len(test_loader)
            test_accuracy *= (100 / len(test_loader.dataset))
            print(f'{name} result: Loss = {test_loss:.4f}, Accuracy = {test_accuracy:.4f}%')
    return test_accuracy


def predict(predict_data_path: str, device: str, transform, model, idx_to_class: dict):
    result_count = 1
    result_data_path = predict_data_path + 'result' + str(result_count) + '/'
    while os.path.isdir(result_data_path):
        result_count += 1
        result_data_path = predict_data_path + 'result' + str(result_count) + '/'
    os.mkdir(result_data_path)
    predict_datasets = os.listdir(predict_data_path)
    for image in predict_datasets:
        if os.path.isfile(predict_data_path + image):
            data = transform(PIL.Image.open(predict_data_path + image)).to(device).unsqueeze(0)
            result = idx_to_class[model(data).max(1)[1].item()]
            if not os.path.isdir(result_data_path + result):
                os.mkdir(result_data_path + result)
            shutil.copy2(predict_data_path + image, result_data_path + result + "/" + image)


def run(variables_file: str) -> None:
    values = get_values(variables_file)
    device = get_device(values['gpu_index'])
    train_loader = get_loader(values['train_data_path'], None, values['batch_size'], values['data_shuffle'])
    total_mean, total_std = get_mean_std(train_loader)
    if values['printable']:
        print(f'Mean = {total_mean}, Standard = {total_std}')
    transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean = total_mean, std = total_std)
    ])
    train_loader = get_loader(values['train_data_path'], transform, values['batch_size'], values['data_shuffle'])
    test_loader = get_loader(values['test_data_path'], transform, values['batch_size'], False)
    models = [timm.create_model(model_name, pretrained = values['model_pretrained'],
        num_classes = values['class_count']) for model_name in values['model_names']]
    if values['load_model']:
        for index, model_name in enumerate(values['model_names']):
            models[index].load_state_dict(torch.load(values['model_path'] + model_name + '.pth'), strict = False)
    criterion = torch.nn.CrossEntropyLoss()
    optimizers = [AdamW(model.parameters(), lr = values['learning_rate']) for model in models]
    schedulers = [CosineAnnealingLR(optimizer, T_max = values['epochs'] // 3) for optimizer in optimizers]
    for name, model, optimizer, scheduler in zip(values['model_names'], models, optimizers, schedulers):
        train(train_loader, device, values['epochs'], name, model, criterion, optimizer, scheduler, values['printable'])
    test_accuracies = []
    for name, model in zip(values['model_names'], models):
        test_accuracies.append(test(test_loader, device, name, model, criterion))
    if values['save_model']:
        if not os.path.isdir(values['model_path']):
            os.mkdir(values['model_path'])
        for model_name, model in zip(values['model_names'], models):
            torch.save(model.state_dict(), values['model_path'] + model_name + '.pth')
    if values['predict_data_path'] != None:
        choose_model = models[test_accuracies.index(max(test_accuracies))]
        idx_to_class = {v: k for k, v in train_loader.dataset.class_to_idx.items()}
        predict(values['predict_data_path'], device, transform, choose_model, idx_to_class)
            

if __name__ == '__main__':
    variables_file = 'variables.yml'
    run(variables_file)